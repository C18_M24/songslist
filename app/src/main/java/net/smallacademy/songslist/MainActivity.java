package net.smallacademy.songslist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Song> songs = new ArrayList<>();
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.songsList);
        songs = new ArrayList<>();
        getjson();
    }

    private void getjson() {

        try {
            String json = readJSONDataFromFile();
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                if (obj != null) {
                    Song song = new Song();
                    song.setTitle(obj.getString("title").toString());
                    song.setArtists(obj.getString("artist".toString()));
                    song.setCoverImage(obj.getString("img_url"));
                    song.setSongURL(obj.getString("web_url"));
                    songs.add(song);
                }
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            adapter = new Adapter(getApplicationContext(), songs);
            recyclerView.setAdapter(adapter);
            Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {

        }
    }

    private String readJSONDataFromFile() throws IOException {
        InputStream inputStream = getAssets().open("songs_json_list.json");
        StringBuilder builder = new StringBuilder();
        try {
            String jsonString = null;
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }
}
